# bMap-mapSmoothMarker

# [演示地址](http://gpscj.lovecd.com)

### 由于这段时间比较忙，新项目没有及时更新，等闲下来了后将演示项目开源。(2018/09/15)
- 由于点比较少 采集GPS APP后期随开源项目一起发布。

### 上面地址是加入新思路的一个demo,所采集的点均由app上报。
### 实现思路
借助百度鹰眼解决如下问题：
1. 纠偏
1. 绑路
1. 去噪
1. 抽稀

这样就大幅度的解决小车在移动的时候根据路线走。
### 项目目前存在已知问题。
1. APP上报点有一定的误差。不能完全相信采集点
1. 因为获取"司机gps"存在网络延迟，所以小车会出现一顿一顿问题。


#### 项目介绍
网约车地图平移方案
- 项目依赖于node+socket提供数据服务
- 前端实现类似滴滴打车小车移动、偏离路线重新规划。
- 由于我这里实际项目中使用的百度地图，所以示例中百度地图方案成熟一些。
- 高德地图本身自己就已经解决了平移问题，如果你项目中使用高德地图只需要 建立一个巡航器 即可，本项目中[index-gd.ejs](./views/index-gd.ejs)也有部分示例。
- 具体实现思路请[点击转移](./help.md)
- 项目核心 [mapSmoothMarker.js](./public/javascripts/mapSmoothMarker.js)
#### 安装教程

1. npm install #安装依赖
2. npm run dev #启动
3. npm run watch #需要 pm2 依赖

#### mapSmoothMarker.js 注解
- 将一个目前坐标的carMk平移到下一个坐标，startCar 函数

```
/**
  * @description 开始移动
  * @param {bMap} map 百度map实例
  * @param {Point} prvePoint 开始坐标点
  * @param {Point} newPoint 结束坐标点
  * @param {marker} marker 标注
  * @param {number} trme 完成动画秒数
  * @param {function} changeFun 每次移动触发的事件
  */
new markerTranslation().startCar(map_, oldPoint, newPoint, carMk, 2000, function (points){
    //每次平移执行的回调-频率 100 毫秒
    // console.log(points);
});//执行动画
```

